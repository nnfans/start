const faker = require('faker')

function userFaker (nData) {
  if (typeof nData !== 'number' || nData < 1) {
    nData = 1
  }
  let users = Array.apply(null, Array(nData))
  users = users.map(() => {
    return {
      name: faker.name.findName(),
      email: faker.internet.email(),
      phone_number: faker.phone.phoneNumberFormat(),
      gender: (Math.random() > 0.5) ? 'M' : 'F',
      createdAt: faker.date.past(),
      updatedAt: faker.date.past()
    }
  })

  return users
}

module.exports = userFaker
